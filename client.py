import socket
import sys

DISCONNECT_MSG = 'DISCONNECT'
FORMAT = 'utf-8'
list_address = []

def add_address(address):
  list_address.append(address)

def check_available_worker():
  print()

# Fitur help
if sys.argv[-1] == '--help':
  print('''
  Usage: python client.py <host> <port> [<argument>]
  Available arguments:
  --av  : available worker
  --st  : monitor status job
  --mn  : 
  --et  : execution time
  --apa : generate APA style
  ''')
  sys.exit(1)

# monitor availibitas worker node(s)
if sys.argv[-1] == '--at':
    # check status node
    print()

# Warning penggunaan fitur
if len(sys.argv) == 1:
  print(
    f"Usage: python {sys.argv[0]} <host> <port> [<argument>]\n\
    Run with --help argument to see a list of available arguments"
  )
  sys.exit(1)

# Memberikan task ke client
if len(sys.argv) == 4:
  HOST = sys.argv[1]
  PORT = int(sys.argv[2])
  ADDRESS = (HOST, PORT)
  print(f"Starting connection to {HOST} on port {PORT}")
  print('masuk')
  client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  client.connect(ADDRESS)

  try:
    argument = sys.argv[3]
    msg = argument

    if argument == '--apa':
      inp = input('Input: ')
      msg += ' ' + inp

    client.send(msg.encode(FORMAT))
    returned_msg = client.recv(1024)
    print('From Server: ', returned_msg.decode(FORMAT))

    client.close()

  except KeyboardInterrupt:
    client.close()
