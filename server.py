import socket
import time

HOST = socket.gethostbyname(socket.gethostname())
PORT = 5050
ADDRESS = (HOST, PORT)
DISCONNECT_MSG = "DISCONNECT"
FORMAT = 'utf-8'

class Server:
    def __init__(self):
        self.__MAX_JOBS = 10
        self.__STATUS = 'READY'
        self.__running_jobs = list()
        self.__num_of_clients = 0

    def get__MAX_JOBS(self):
        return self.__MAX_JOBS

    def set_status(self, new_status):
        self.__status = new_status

    def check_availibity(self) -> bool:
        if len(self.__running_jobs) == self.__MAX_JOBS:
            # TODO
            self.set_status('BUSY')
            return False
        else:
            return True

    def add_running_jobs(self, client):
        if self.check_availibity():
            self.__running_jobs.append(client)

    def remove_jobs(self, client):
        self.__running_jobs.remove(client)

    def get_jobs(self):
        return self.__running_jobs

    def increment_client(self):
        self.__num_of_clients += 1

    def get_num_of_clients(self) -> int:
        return self.__num_of_clients

class Client:
    def __init__(self, address):
        self.__address = address
        self.__status = 'RUNNING'
        self.__exec_time = 0.0
        self.__type = ''

    def get_address(self):
        return self.__address

    def get_status(self):
        return self.__status

    def set_status(self, new_status):
        self.__status = new_status

    def set_exec_time(self, new_time):
        self.__exec_time = new_time

    def get_exec_time(self):
        return self.__exec_time

    def set_type(self, new_type):
        self.__type = new_type

    def get_type(self):
        return self.__type


class ClientLog:
    def __init__(self):
        self.__log = []

    def add_log(self, client: Client):
        self.__log.append(client)

    def get_log(self):
        return self.__log


server_ins = Server()
log = ClientLog()

def process_argument(argument: str):
    split_argument = argument.split(' ')

    if split_argument[0] == '--st':
        return get_status_jobs()
    elif split_argument[0] == '--et':
        return get_execution_types()
    elif split_argument[0] == '--apa':
        return apa_gen(split_argument[1])
    else:
        return 'Invalid argument'

def accept_wrapper(server):
    # Accept connection from client
    while True:
        connection, address = server.accept()
        print(f'CONNECTION ACCEPTED FROM {address}')

        client = Client(address=address)

        service_connection(connection, client)


def service_connection(conn, client):
    # Receiving data from client
    recv_data = conn.recv(1028)
    recv_data = recv_data.decode(FORMAT)

    if recv_data == DISCONNECT_MSG:
        conn.close()

    start = time.perf_counter()

    try:
        split_argument = recv_data.split(' ')

        # Perform job
        if split_argument[0] == '--st':
            result = get_status_jobs()
        elif split_argument[0] == '--et':
            result = get_execution_types()
        elif split_argument[0] == '--apa':
            result = apa_gen(split_argument[1])

        # check execution time
        if result == 'Invalid argument':
            client.set_status('FAILED')
            finish = time.perf_counter()
        else:
            result = result.encode(FORMAT)
            client.set_status('FINISHED')
            finish = time.perf_counter()
            conn.send(result)
    except KeyboardInterrupt:
        conn.close()
    except:
        client.set_status('FAILED')
        finish = time.perf_counter()
        conn.close()

    # Klasifikasi job berdasarkan waktu eksekusi
    duration = round(finish - start, 3)
    client.set_exec_time(duration)

    if duration < 1.0:
        client.set_exec_time('SANGAT SINGKAT')
    elif duration >= 60.0:
        client.set_exec_time('LAMA')
    else:
        client.set_exec_time('SINGKAT')

    log.add_log(client)

# Check job status yang sedang dieksekusi
def get_status_jobs():
    result = f''
    for job in log.get_log():
        result += f'{job.get_address()}: {job.get_status()}\n'

    return result

# Check variasi job berdasarkan waktu eksekusi
def get_execution_types():
    result = f''
    for job in log.get_log():
        result += f'{job.get_address()}: {job.get_exec_time()} {job.get_type()}\n'

    return result

# Perform APA style generator
def apa_gen(string):
    list_input = string.split('|')
    return edit_name(list_input[0]) + edit_year(list_input[1]) + edit_title(list_input[2]) + edit_pub(list_input[3]) + \
           list_input[4] + '.'

def edit_name(name):
    splitted_name = name.split('_')
    return splitted_name[1] + ', ' + splitted_name[0][0] + '. '

def edit_year(year):
    return '(' + year + ')' + '. '

def edit_title(title):
    return title + '. '

def edit_pub(pub):
    return pub + ': '


# initiate host and port
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDRESS)
print(f'LISTENING ON {HOST}, {PORT}')
server.listen()
accept_wrapper(server)
